<?php
/**
 * @file
 * Define environment_libraries module functions.
 */

// # Environment Libraries Module
module_load_include('inc', 'environment_libraries', 'environment_libraries.admin');
module_load_include('inc', 'environment_libraries', 'environment_libraries.blocks');
module_load_include('inc', 'environment_libraries', 'environment_libraries.context');
module_load_include('inc', 'environment_libraries', 'environment_libraries.class');
module_load_include('inc', 'environment_libraries', 'environment_libraries.entity');
module_load_include('inc', 'environment_libraries', 'environment_libraries.features');
module_load_include('inc', 'environment_libraries', 'environment_libraries.form');
module_load_include('inc', 'environment_libraries', 'environment_libraries.form_element');
module_load_include('inc', 'environment_libraries', 'environment_libraries.libraries');
module_load_include('inc', 'environment_libraries', 'environment_libraries.permissions');
module_load_include('inc', 'environment_libraries', 'environment_libraries.theme');
use Environment_Libraries\LibraryUtils;

// ## Environment Functions
/**
 * Define environments from highest to lowest priority ie. (production => staging => dev).
 *
 * @return array
 *   environments
 */
function environment_libraries_environments() {
  return LibraryUtils::get_environments();
}

/**
 * Get the current site (or library) environment.
 *
 * @param object $library
 *    Library entity.
 *
 * @return string
 *   environment
 */
function environment_libraries_environment_current($library = NULL) {
  return LibraryUtils::current_environment($library);
}

/**
 * Download/Synchronize/Cache tracking code file locally.
 *
 * Modified from google analytics module by way of Acquia javascript_libraries.
 *
 * @param string $uri
 *   The full URL to the external javascript file.
 * @param string $lid
 *   Library id.
 * @param bool $sync_cached_file
 *   Synchronize tracking code and update if remote file have changed.
 *
 * @return mixed
 *   The path to the local javascript file on success, boolean FALSE on failure.
 */
function environment_libraries_cache($uri, $lid = NULL, $sync_cached_file = FALSE) {
  // ## Library Remote File Local Cache
  // Create a local file cache of remote files in the
  // environment_libraries folder.
  $path = 'public://environment_libraries';

  $uri = LibraryUtils::url_path($uri);
  $ext = LibraryUtils::url_ext($uri);

  $file_destination = $path . '/env_' . drupal_hash_base64($uri) . $ext;
  $file_exists = file_exists($file_destination);
  // If the file exists, we can assume success.
  $success = $file_exists;

  if (!$file_exists || $sync_cached_file) {
    // Download the library.
    $LOCK_CID = __FUNCTION__ . $file_destination;
    if( lock_may_be_available($LOCK_CID) && lock_acquire($LOCK_CID) ){
      try{
        if(file_exists($file_destination)){ return $file_destination; }
        $uri = LibraryUtils::http_url($uri);
        $result = drupal_http_request($uri);

        if ($result->code != 200 || empty($result->data)) {
          watchdog('environment_libraries', 'Error fetching remote file @uri | @destination | HTTP code: @code.', array('@uri' => $uri, '@destination' => $file_destination, '@code' => $result->code), WATCHDOG_WARNING);
          // If the file could not be retrieved, save it as blank so as to not retry.
          environment_libraries_file_unmanaged_save_data("", $file_destination);
          return FALSE;
        }
        if ($file_exists) {
          // Only do a (potentially somewhat slow/expensive) file write
          // if the content changed.
          $current_contents = file_get_contents($file_destination);
          if ($result->data != $current_contents) {
            // Save updated javascript file to disk.
            $success = environment_libraries_file_unmanaged_save_data($result->data, $file_destination);
            if($ext == '.js'){
              drupal_clear_js_cache();
            }else if($ext == '.css'){
              drupal_clear_css_cache();
            }
            watchdog('environment_libraries', 'Locally cached environment library @pid | @uri | @destination has been updated.', array('@pid' => getmypid(), '@uri' => $uri, '@destination' => $file_destination), WATCHDOG_INFO);
            module_invoke('environment_libraries_cache', $uri, $lid);
          }
        } else if (file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
          // There is no need to flush JS here as core refreshes JS caches
          // automatically if new files are added.
          $success = environment_libraries_file_unmanaged_save_data($result->data, $file_destination);
          //watchdog('environment_libraries', 'Locally cached environment library @pid | @uri | @destination has been saved.', array('@pid' => getmypid(), '@uri' => $uri, '@destination' => $file_destination), WATCHDOG_INFO);
        }
      } finally {
        lock_release($LOCK_CID);
      }
    }else{
      // We must wait until the lock is released for the file to be regenerated.
      watchdog('environment_libraries', 'Library rebuild lock hit: pid | @uri | @destination', array('@pid' => getmypid(),'@uri' => $uri, '@destination' => $file_destination), WATCHDOG_INFO);
      lock_wait($LOCK_CID, 60);
    }
  }

  // Return the local JS file path or FALSE.
  return ( $success ? $file_destination : FALSE );
}

/**
 * @param $library
 *   A library to convert to local cached files.
 */
function environment_libraries_cache_external_library(&$library, $sync = FALSE) {
  foreach( array('js', 'css') as $type ){
    if(!isset($library[$type])){ continue; }
    foreach( $library[$type] as $file => $settings ){
      if(!isset($settings['type']) || !isset($settings['cache']) ||
         $settings['type'] !== 'external' && $settings['cache'] !== TRUE ){ continue; }
      if(isset($settings['original'])){
        $local_file = environment_libraries_cache($settings['original'], NULL, $sync);
      }else{
        $local_file = environment_libraries_cache($file, NULL, $sync);
      }
      if($local_file !== FALSE){
        unset($library[$type][$file]);
        $settings['type'] = 'file';
        $settings['original'] = $file;
        $library[$type][$local_file] = $settings;
        $library['#environment_libraries_cache'] = TRUE;
      }
    }
  }
}

/**
 * Save environment library file.
 *
 * @see javascript_libraries: javascript_libraries_file_unmanaged_save_data()
 */
function environment_libraries_file_unmanaged_save_data($data, $destination) {
  // Create a temporary file and then move it in place.
  $destination_tmp = $destination . '-tmp';
  if (file_unmanaged_save_data($data, $destination_tmp, FILE_EXISTS_REPLACE)) {
    return @rename($destination_tmp, $destination);
  }
  return FALSE;
}

/**
 * Implements hook_preprocess_page().
 */
function environment_libraries_preprocess_page(&$variables, $hook) {
  // Add Page-level module css / js.
  if (path_is_admin(current_path())) {
    $module_path = drupal_get_path("module", "environment_libraries");
    drupal_add_css($module_path . '/css/environment_libraries.css',
      array('type' => 'file', 'scope' => 'header', 'group' => CSS_THEME));
    drupal_add_js($module_path . '/js/environment_libraries.admin.js',
      array('type' => 'file', 'scope' => 'footer'));
  }
}

// ## Cron, cache clear, drush, etc
/**
 * Rebuild the library cache files.
 *
 * Implements hook_cron().
 */
function environment_libraries_cron() {
  // TODO re-enable cron cached file rebuild, expose as option in settings.
  //if(module_exists('elysia_cron')){
  //  environment_libraries_flush_caches();
  //}else{
  //  $last = intval(variable_get('environment_libraries_last_sync', 0));
  //  $interval = intval(variable_get('environment_libraries_cron_interval', 86400));
  //  if(REQUEST_TIME > $last + $interval){
  //    environment_libraries_flush_caches();
  //    variable_set('environment_libraries_last_sync', REQUEST_TIME);
  //  }
  //}
}

/**
 * Implements hook_flush_caches().
 */
function environment_libraries_flush_caches() {
  // Delete the cached libraries
  // @see https://api.drupal.org/api/drupal/includes%21file.inc/function/file_unmanaged_delete_recursive/7.x
  $path = 'public://environment_libraries';
  if (is_dir($path)) {
    $dir = dir($path);
    while (($entry = $dir->read()) !== FALSE) {
      if ($entry == '.' || $entry == '..') {
        continue;
      }
      $file = $path . '/' . $entry;
      if(is_file($file)){
        file_unmanaged_delete($file);
      }
    }
    $dir->close();
  }

  // And re-download them.
  $env_libraries = environment_libraries_library_load_multiple(FALSE);
  foreach ($env_libraries as $library) {
    // Rebuild the cached file version of this asset.
    $library_files = LibraryUtils::list_files($library);
    foreach ($library_files as $library_file) {
      if(isset($library_file['cache']) && $library_file['cache'] === '1'){
        environment_libraries_cache($library_file['url'], $library->lid, TRUE);
      }
    }
  }

  // Search for all cached environment libraries
  foreach(module_list() as $module){
    foreach(drupal_get_library($module) as $library){
      if(isset($library['#environment_libraries_cache'])){
        // and re-sync the cached version of the library.
        environment_libraries_cache_external_library($library, TRUE);
      }
    }
  }

  // Clear our entity cache.
  return array('cache_entity_environment_libraries_library');
}

function environment_libraries_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'flush_caches') {
    $group = $implementations['environment_libraries'];
    unset($implementations['environment_libraries']);
    $implementations = array( 'environment_libraries' => $group ) + $implementations;
    //$implementations['environment_libraries'] = $group;
  }
}

/**
 * Implements hook_ctools_plugin_api().
 */
function environment_libraries_ctools_plugin_api($owner, $api) {
  if ($owner == 'environment_libraries' && $api == 'default_environment_libraries_library') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_help().
 */
function environment_libraries_help($path, $arg) {

  switch ($path) {
    case 'admin/help#environment_libraries':
      $filepath = dirname(__FILE__) . '/README.md';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }
      else {
        $filepath = dirname(__FILE__) . '/README.txt';
        if (file_exists($filepath)) {
          $readme = file_get_contents($path);
        }
      }
      if (!isset($readme)) {
        return NULL;
      }
      $readme = filter_xss_admin($readme);
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }
        else {
          $output = '<pre>' . filter_xss_admin($readme) . '</pre>';
        }
      }
      else {
        $output = '<pre>' . filter_xss_admin($readme) . '</pre>';
      }
      return $output;
  }
}
