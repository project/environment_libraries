/**
 * @file environment_libraries.admin.js
 * Environment Libraries Administrative UI.
 */

(function($){
	"use strict";
	Drupal.behaviors.environment_libraries_admin = {
		attach: function(context){
			var $name = $('#edit-environment-libraries-library .form-item-environment-libraries-library-name input', context),
				$machine_name = $('#edit-environment-libraries-library .form-item-environment-libraries-library-machine-name input', context);

			$name.blur(function(){
				$machine_name.val($name.val().toLowerCase().replace(/[^a-z0-9_]+/g,'_'));
			});

			$('.form-type-environment-library fieldset.environment-library--file', context).each(function(){
				var $fieldset = $(this);

				// Minification & Aggregation require caching, auto-check this dependency.
				function check($cache, $opt){
					return function(){
						if ($opt.find("input").is(":checked")) {
							$cache.find("input").attr("checked", true);
						}
					};
				}
				function uncheck($cache, $opt){
					return function(){
						if ($cache.find("input").is(":checked")) {
							$opt.find("input").attr("checked", false);
						}
					};
				}
				$fieldset.find(".environment").each(function(){
					var $env = $(this),
						$options = $env.find(".form-type-checkbox"),
						$cache = $($options[0]),
						$agg = $($options[1]),
						$minify = $($options[2]);
					$minify.bind("click", check($cache, $minify));
					$agg.bind("click", check($cache, $agg));
					$cache.bind("click", uncheck($cache, $agg));
					$cache.bind("click", uncheck($cache, $minify));

					// Copy the title attribute to the checkbox labels.
					$options.each(function(){
						var $this = $(this),
							$input = $this.find("input"),
							$lbl = $this.find("label");
						$lbl.attr("title", $input.attr("title"));
					});
				});
			});
		}
	}
})(jQuery);
